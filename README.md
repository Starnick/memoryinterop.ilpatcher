![alt text](https://bytebucket.org/Starnick/memoryinterop.ilpatcher/raw/513bbb4fc084b200ae2de5fcfe8a2e7caac1c1a3/logo_256.png "MemoryInterop.ILPatcher Logo")

**The latest release can be downloaded via [NuGet](https://www.nuget.org/packages/MemoryInterop.ILPatcher).** 


[![Build Status](https://dev.azure.com/nicholaswoodfield/OSS-Libraries/_apis/build/status/bitbucket/starnick/MemoryInterop.ILPatcher?branchName=master)](https://dev.azure.com/nicholaswoodfield/OSS-Libraries/_build/latest?definitionId=7&branchName=master)

## Introduction ##

This project is a MSBuild task that runs after compilation to inject IL code that cannot be generated from C# code. A good example is the **cpblk** instruction (memcpy). The primary goal
of this tool is to allow for generic-typed methods (managed types) that can interop with (unmanaged) memory pointers, and do it as optimally as possible. This is crucial for apps that need to work "low-level" such
as a graphics application, without having to drop down to native code.

The patcher works by scanning calls to stub methods declared in a type called **MemoryInterop**. Calls are intercepted and IL is injected. Some methods are not inline and will replace the entire method body (inlined
methods are self documenting). When the NuGet package is installed, the stub type is automatically installed as a content file (newer PackageReference format will hide it as an immutable file in your intermediate folder).

So you'll be able to write generic code like:

```csharp
        int expected = 10;
        IntPtr ptr = MemoryInterop.AsPointerInline<int>(ref expected);

        int ret = MemoryInterop.ReadInline<int>(ptr);
```

or

```csharp
        public static ref T AsRef<T>(IntPtr pSrc) where T : struct
        {
            return ref MemoryInterop.AsRef<T>(pSrc);
        }
```

and the tool will take care of the rest. Pointers can be to anything, the unmanaged heap, pinned memory, stack, etc. No boxing, no mess either.

## Supported Platforms ##

The NuGet package is intended to be a build-time dependency that can run cross-platform, therefore it targets **.NET Standard 2.0** and uses **C# 7 features (ref returns)**. This means SDKs for .NET Core 2.0+ or .NET Framework 4.7+ are required to be 
installed in your build environment. Assemblies that target earlier runtime versions can of course be patched, the only real limit is what is supported by Mono.Cecil (which the task uses to do the IL injection). 

The version of Mono.Cecil shipped with the tool does **NOT** support .NET Framework 2.0 or the Compact Framework. So the minimum targets that are supported are:

* .NET Standard 1.1+
* .NET Framework 3.5+
* .NET Core 1.0+

The tool has been tested in both the new dotnet CLI (.net core command line) and Visual Studio 2017 build environments. The NuGet package was specifically designed to support both the new PackageReference and
Package .config formats. So far the tool has been tested in these environments only on a Windows machine, so if you see different behavior on Linux/Mac .NET Core environments, please file a defect!

## Licensing ##

The tool is licensed under the [MIT](https://opensource.org/licenses/MIT) license. This means you're free to modify the source and use the tool in whatever way you want, as long as you attribute the original authors.

## Contact ##

Follow project updates and more on [Twitter](https://twitter.com/Tesla3D/).

In addition, check out these other projects (all powered by MemoryInterop) from the same author:

[AssimpNet](https://bitbucket.org/Starnick/assimpnet) - A wrapper for the Open Asset Import Library.

[TeximpNet](https://bitbucket.org/Starnick/teximpnet) - A wrapper for the Nvidia Texture Tools and FreeImage libraries.

[Tesla Graphics Engine](https://bitbucket.org/Starnick/tesla3d) - A 3D rendering engine written in C# and the primary driver for tools such as these.

----
  
#### Logo ####

[Weaving](http://thenounproject.com/term/weaving/21753) designed [Lisa Staudinger](http://thenounproject.com/TheGreenChicken/) from the [Noun Project](http://thenounproject.com)