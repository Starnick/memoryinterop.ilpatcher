﻿/*
* Copyright (c) 2017-2020 MemoryInterop.ILPatcher - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.IO;

namespace MemoryInterop.ILPatcher
{
    /// <summary>
    /// Assembly resolver for Cecil. We take build references from MSBuild, so we know the location of all the assembly references already, but we load them as needed.
    /// </summary>
    public class PatcherAssemblyResolver : IAssemblyResolver
    {
        private struct AssemblyEntry
        {
            public String FullPath;
            public AssemblyDefinition Definition;

            public AssemblyEntry(String path)
            {
                FullPath = path;
                Definition = null;
            }
        }

        private bool m_isDisposed;
        private Dictionary<String, AssemblyEntry> m_assemblies;
        private IPatcherLogger m_logger;

        /// <summary>
        /// Constructs a new <see cref="PatcherAssemblyResolver"/>.
        /// </summary>
        /// <param name="msbuildReferences">List of assembly references from msbuild, seperated by semicolons.</param>
        /// <param name="logger">Logger for messages.</param>
        public PatcherAssemblyResolver(String msbuildReferences, IPatcherLogger logger)
        {
            m_logger = logger;
            m_assemblies = new Dictionary<String, AssemblyEntry>();

            BuildReferenceTable(msbuildReferences);
        }

        /// <summary>
        /// Resolves the specified assembly name.
        /// </summary>
        /// <param name="name">Assembly name to resolve.</param>
        /// <returns>The resolved assembly.</returns>
        /// <exception cref="AssemblyResolutionException">Thrown if the assembly could not be resolved.</exception>
        public AssemblyDefinition Resolve(AssemblyNameReference name)
        {
            return Resolve(name, null);
        }

        /// <summary>
        /// Resolves the specified assembly name.
        /// </summary>
        /// <param name="name">Assembly name to resolve.</param>
        /// <param name="parameters">Parameters for reading the assembly, if null a default one will be generated.</param>
        /// <returns>The resolved assembly.</returns>
        /// <exception cref="AssemblyResolutionException">Thrown if the assembly could not be resolved.</exception>
        public AssemblyDefinition Resolve(AssemblyNameReference name, ReaderParameters parameters)
        {
            if (parameters == null)
                parameters = new ReaderParameters();

            //Ensure we're set as the resolver...
            if (parameters.AssemblyResolver != this)
                parameters.AssemblyResolver = this;

            AssemblyEntry resolvedAssembly;
            if(m_assemblies.TryGetValue(name.Name, out resolvedAssembly))
            {
                if (resolvedAssembly.Definition == null)
                {
                    resolvedAssembly.Definition = AssemblyDefinition.ReadAssembly(resolvedAssembly.FullPath, parameters);
                    m_assemblies[name.FullName] = resolvedAssembly;

                    m_logger.LogInfo("Resolved AsssemblyReference '{0}'", name.FullName);
                }
            }

            if (resolvedAssembly.Definition == null)
            {
                m_logger.LogError("Unable to resolve AssemblyReference '{0}'", name.FullName);

                throw new AssemblyResolutionException(name);
            }

            return resolvedAssembly.Definition;
        }

        /// <summary>
        /// Disposes of any cached assembly references.
        /// </summary>
        /// <param name="disposing">True if dispose was called.</param>
        protected virtual void Dispose(bool disposing)
        {
            if(m_isDisposed)
            {
                m_isDisposed = true;
            }
        }

        /// <summary>
        /// Disposes of any cached assembly references.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        private void BuildReferenceTable(String references)
        {
            if (String.IsNullOrEmpty(references))
                return;

            String[] dllReferences = references.Split(';');
            foreach (String dllRef in dllReferences)
            {
#if DEBUG
                m_logger.LogInfo("Reference: {0}", dllRef);
#endif
                m_assemblies.Add(Path.GetFileNameWithoutExtension(dllRef), new AssemblyEntry(dllRef));
            }
        }
    }
}
