﻿/*
* Copyright (c) 2017-2020 MemoryInterop.ILPatcher - Nicholas Woodfield
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Mono.Cecil;
using Mono.Cecil.Pdb;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace MemoryInterop.ILPatcher
{
    /// <summary>
    /// MSBuild task for patching an assembly with memory interop functionality.
    /// </summary>
    public class PatcherTask : Task, IPatcherLogger
    {
        /// <summary>
        /// Gets or sets msbuild references for the assembly. Required as its how we resolve assembly references for Mono.Cecil.
        /// </summary>
        [Required]
        public String BuildReferences { get; set; }

        /// <summary>
        /// Gets or sets the target assembly file path. This points to the assembly to be patched typically in the output folder.
        /// </summary>
        [Required]
        public String TargetAssemblyFilePath { get; set; }

        /// <summary>
        /// Gets or sets the strongname key file that will be used for strong name signing after modification. This is optional.
        /// </summary>
        public String KeyFilePath { get; set; }

        /// <summary>
        /// Executes patching task.
        /// </summary>
        /// <returns>True if patching was successful, false if otherwise.</returns>
        public override bool Execute()
        {
            LogInfo(String.Format("BEGIN patching assembly '{0}'", Path.GetFileName(TargetAssemblyFilePath)));

            //Setup reader/writer parameters
            ReaderParameters readParams = new ReaderParameters();
            WriterParameters writeParams = new WriterParameters();

            String pdbFile = Path.Combine(Path.GetDirectoryName(TargetAssemblyFilePath), Path.GetFileNameWithoutExtension(TargetAssemblyFilePath) + ".pdb");

            if(File.Exists(pdbFile))
            {
                LogInfo("Symbols file found, will write out a new PDB file.");

                readParams.SymbolReaderProvider = new PdbReaderProvider();
                readParams.ReadSymbols = true;

                writeParams.SymbolWriterProvider = new PdbWriterProvider();
                writeParams.WriteSymbols = true;
            }
            else
            {
                LogInfo("Symbols file not found.");
            }

            if(File.Exists(KeyFilePath))
            {
                LogInfo("Strong name key found. Signing assembly with '{0}'.", Path.GetFileName(KeyFilePath));

                using (FileStream fs = File.OpenRead(KeyFilePath))
                    writeParams.StrongNameKeyPair = new StrongNameKeyPair(fs);
            }
            else
            {
                LogInfo("Strong name key not found. Assembly will not be signed.");
            }


            //Custom resolver based on reference paths from msbuild
            readParams.AssemblyResolver = new PatcherAssemblyResolver(BuildReferences, this);
            readParams.ReadWrite = true;

            //Go through each module (usually just one) and look for types to patch.
            using (AssemblyDefinition targetAssembly = AssemblyDefinition.ReadAssembly(TargetAssemblyFilePath, readParams))
            {
                bool changed = false;
                foreach (ModuleDefinition targetModule in targetAssembly.Modules)
                    changed |= PatchModule(targetModule, readParams.AssemblyResolver);

                if (changed)
                {
                    targetAssembly.Write(writeParams);

                    if (targetAssembly.MainModule.SymbolReader != null)
                    {
                        if (!(targetAssembly.MainModule.SymbolReader is Mono.Cecil.Cil.PortablePdbReader))
                            LogInfo("Writing new Non-Portable PDB.");
                        else
                            LogInfo("Writing new Portable PDB.");
                    }
                }
                else
                {
                    LogInfo("ILPatcher didn't find anything to patch, skipping Assembly and PDB writing.");
                }
            }

            LogInfo(String.Format("DONE patching assembly '{0}'", Path.GetFileName(TargetAssemblyFilePath)));

            return true;
        }

        /// <summary>
        /// Write inlined method.
        /// </summary>
        public void LogError(String msg)
        {
            Log.LogMessage(MessageImportance.High, String.Format("\t>> ERROR: {0}", msg));
        }

        /// <summary>
        /// Log an error message.
        /// </summary>
        /// <param name="format">Format of the message.</param>
        /// <param name="variables">Arguments for String.Format</param>
        public void LogError(String format, params object[] variables)
        {
            Log.LogMessage(MessageImportance.High, String.Format("\t>> ERROR: {0}", String.Format(format, variables)));
        }

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="msg">Text to write out.</param>
        public void LogWarn(String msg)
        {
            Log.LogMessage(MessageImportance.High, String.Format("\t>> WARNING: {0}", msg));
        }

        /// <summary>
        /// Log a warning message.
        /// </summary>
        /// <param name="format">Format of the message.</param>
        /// <param name="variables">Arguments for String.Format</param>
        public void LogWarn(String format, params object[] variables)
        {
            Log.LogMessage(MessageImportance.High, String.Format("\t>> WARNING: {0}", String.Format(format, variables)));
        }

        /// <summary>
        /// Log an info message.
        /// </summary>
        /// <param name="msg">Text to write out.</param>
        public void LogInfo(String msg)
        {
            Log.LogMessage(MessageImportance.High, String.Format("\t>> INFO: {0}", msg));
        }

        /// <summary>
        /// Log an info message.
        /// </summary>
        /// <param name="format">Format of the message.</param>
        /// <param name="variables">Arguments for String.Format</param>
        public void LogInfo(String format, params object[] variables)
        {
            Log.LogMessage(MessageImportance.High, String.Format("\t>> INFO: {0}", String.Format(format, variables)));
        }

        #region Patching Logic

        private bool PatchModule(ModuleDefinition module, IAssemblyResolver resolver)
        {
            IReadOnlyDictionary<String, IReadOnlyList<PatchMethodProcessor>> patchProcessors = GetProcessors();
            PatcherContext context = new PatcherContext(module, this);

            bool changed = false;

            foreach (TypeDefinition typeDef in module.Types)
                changed |= PatchType(typeDef, patchProcessors, context);

            changed |= RemoveReference(module);
            changed |= RemoveInternalTypes(patchProcessors.Keys, module);

            return changed;
        }

        private bool PatchType(TypeDefinition typeDef, IReadOnlyDictionary<String, IReadOnlyList<PatchMethodProcessor>> patchProcessors, PatcherContext context)
        {
            bool changed = false;

            //Look at methods
            foreach (MethodDefinition method in typeDef.Methods)
                changed |= PatchMethodProcessor.ProcessMethod(method, patchProcessors, context);

            //Look at properties
            foreach (PropertyDefinition property in typeDef.Properties)
            {
                if (property.GetMethod != null)
                    changed |= PatchMethodProcessor.ProcessMethod(property.GetMethod, patchProcessors, context);

                if (property.SetMethod != null)
                    changed |= PatchMethodProcessor.ProcessMethod(property.SetMethod, patchProcessors, context);
            }

            //Look at nested types
            foreach (TypeDefinition nestedTypeDef in typeDef.NestedTypes)
                changed |= PatchType(nestedTypeDef, patchProcessors, context);

            return changed;
        }

        private bool RemoveReference(ModuleDefinition module)
        {
            AssemblyNameReference memInteropReference = module.AssemblyReferences.FirstOrDefault(x => x.Name.Equals("MemoryInterop.ILPatcher"));
            if (memInteropReference == null)
                return false;

            bool changed = module.AssemblyReferences.Remove(memInteropReference);
            LogInfo(String.Format("Removing reference to '{0}'.", memInteropReference.Name));
            return changed;
        }

        private bool RemoveInternalTypes(IEnumerable<String> typeNames, ModuleDefinition module)
        {
            bool changed = false;

            List<TypeDefinition> typesToRemove = new List<TypeDefinition>();

            foreach (TypeDefinition typeDef in module.Types)
            {
                foreach (String typeName in typeNames)
                {
                    if (typeDef.Name.Equals(typeName))
                    {
                        typesToRemove.Add(typeDef);
                        break;
                    }
                }
            }

            foreach (TypeDefinition typeDef in typesToRemove)
            {
                LogInfo(String.Format("Removing Type '{0}'", typeDef.FullName));
                changed |= module.Types.Remove(typeDef);
            }

            typesToRemove.Clear();
            return changed;
        }

        private IReadOnlyDictionary<String, IReadOnlyList<PatchMethodProcessor>> GetProcessors()
        {
            Dictionary<String, IReadOnlyList<PatchMethodProcessor>> processors = new Dictionary<String, IReadOnlyList<PatchMethodProcessor>>();
            MemoryInteropProcessors.CreateProcessors(processors);

            return processors;
        }

        #endregion
    }
}
